<?php

class Date {

    // Возвращает месяц на русском языке в зависимоти от метки времени (timestamp)
    public static function getMonth($timestamp) {

        $string = $timestamp;
        $pattern = '/([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/';
        $allMonths = '$2';
        $months = preg_replace($pattern, $allMonths, $string);
        $replacement = '$3';
        switch ($months) {
            case 1: $month = 'Январь' . '<br>';
                break;
            case 2: $month = 'Февраль' . '<br>';
                break;
            case 3: $month = 'Март' . '<br>';
                break;
            case 4: $month = 'Апрель' . '<br>';
                break;
            case 5: $month = 'Май' . '<br>';
                break;
            case 6: $month = 'Июнь' . '<br>';
                break;
            case 7: $month = 'Июль' . '<br>';
                break;
            case 8: $month = "Август" . '<br>';
                break;
            case 9: $month = 'Сентябрь' . '<br>';
                break;
            case 10: $month = 'Октябрь' . '<br>';
                break;
            case 11: $month = 'Ноябрь' . '<br>';
                break;
            case 12: $month = 'Декабрь' . '<br>';
                break;
        }

        return $month;
    }

    public static function getDay($timestamp) {
        $string = $timestamp;
        $pattern = '/([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/';
        $replacement = '$3';

        return preg_replace($pattern, $replacement, $string);
    }

}
