<?php

class Blog {

    const SHOW_BY_DEFAULT = 3;

    // Возвращает одну новость блога по id
    public static function getNewsItemByID($id) {
        $id = intval($id);

        if ($id) {

            $db = Db::getConnection();
            $result = $db->query('SELECT * FROM news WHERE id=' . $id);

            $result->setFetchMode(PDO::FETCH_ASSOC);

            $newsItem = $result->fetch();

            return $newsItem;
        }
    }

    // Возвращает список новостей блога по категориям
    public static function getNewsListByCategory($categoryId = false, $page = 1) {
        if ($categoryId) {

            $page = intval($page);
            $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

            $db = Db::getConnection();
            $newsList = array();

            if ($categoryId == 1) {
                $result = $db->query("SELECT * FROM news "
                        . "WHERE status = '1' "
                        . "ORDER BY date ASC "
                        . "LIMIT " . self::SHOW_BY_DEFAULT
                        . ' OFFSET ' . $offset);
            } else {
                $result = $db->query("SELECT * FROM news "
                        . "WHERE status = '1' AND category_id = '$categoryId' "
                        . "ORDER BY id ASC "
                        . "LIMIT " . self::SHOW_BY_DEFAULT
                        . ' OFFSET ' . $offset);
            }

            $i = 0;
            while ($row = $result->fetch()) {
                $newsList[$i]['id'] = $row['id'];
                $newsList[$i]['title'] = $row['title'];
                $newsList[$i]['date'] = $row['date'];
                $newsList[$i]['short_content'] = $row['short_content'];
                $newsList[$i]['content'] = $row['content'];
                $newsList[$i]['author_name'] = $row['author_name'];
                $newsList[$i]['preview'] = $row['preview'];
                $newsList[$i]['type'] = $row['type'];
                $newsList[$i]['category_id'] = $row['category_id'];
                $newsList[$i]['status'] = $row['status'];
                $i++;
            }

            return $newsList;
        }
    }

    // Возвращает количество новостей блога по категориям
    public static function getTotalNewsInCategory($categoryId) {
        $db = Db::getConnection();

        if ($categoryId == 1) {
            $result = $db->query('SELECT count(id) AS count FROM news '
                    . 'WHERE status="1" ');
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $row = $result->fetch();
        } else {
            $result = $db->query('SELECT count(id) AS count FROM news '
                    . 'WHERE status="1" AND category_id ="' . $categoryId . '"');
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $row = $result->fetch();
        }

        return $row['count'];
    }

}
