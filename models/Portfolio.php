<?php

class Portfolio {

    const SHOW_BY_DEFAULT = 6;

    // Возвращает список новостей портфолио
    public static function getAllPortfolioNews($page = 1) {

        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $newsList = array();

        $result = $db->query('SELECT * FROM portfolio_news '
                . "WHERE status = '1' "
                . "LIMIT " . self::SHOW_BY_DEFAULT
                . ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {
            $newsList[$i]['id'] = $row['id'];
            $newsList[$i]['category_id'] = $row['category_id'];
            $newsList[$i]['title'] = $row['title'];
            $newsList[$i]['short_content'] = $row['short_content'];
            $newsList[$i]['content'] = $row['content'];
            $newsList[$i]['preview'] = $row['preview'];            

            $i++;
        }

        return $newsList;
    }

    // Возвращает одну новость портфолио по id
    public static function getPortfolioPhotoUrl() {
        $db = Db::getConnection();

        $photoUrl = array();

        $result = $db->query('SELECT portfolio_news.id, photo_url FROM portfolio_news JOIN portfolio_photo ON portfolio_news.id = portfolio_photo.news_id');

        $i = 0;
        while ($row = $result->fetch()) {
            $photoUrl[$i]['id'] = $row['id'];
            $photoUrl[$i]['photo_url'] = $row['photo_url'];
            $i++;
        }

        return $photoUrl;
    }

    // Возвращает количество новостей портфолио
    public static function getTotalNewsInPortfolio() {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM portfolio_news '
                . 'WHERE status="1" ');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();


        return $row['count'];
    }
    
    public static function getPortfolioCategoryNameById() {
        $db = Db::getConnection();

        $portfolioCategory = array();

        $result = $db->query('SELECT filter, name FROM portfolio_news JOIN portfolio_category ON portfolio_news.category_id = portfolio_category.id');

        $i = 0;
        while ($row = $result->fetch()) {
            $portfolioCategory[$i]['filter'] = $row['filter'];
            $portfolioCategory[$i]['name'] = $row['name'];
            $i++;
        }

        return $portfolioCategory;
    }
    
    public static function getLastPortfolioNews() {
        $db = Db::getConnection();

        $lastPortfolioNews = array();

        $result = $db->query('SELECT preview FROM portfolio_news WHERE status = 1 ORDER BY id DESC LIMIT 6');

        $i = 0;
        while ($row = $result->fetch()) {
            $lastPortfolioNews[$i]['preview'] = $row['preview'];
            $i++;
        }

        return $lastPortfolioNews;
    }

}
