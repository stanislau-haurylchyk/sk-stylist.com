<?php

class Review {

    const SHOW_BY_DEFAULT = 3;

    // Возвращает список отзывов
    public static function getAllReviews($page = 1) {

        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $reviewsList = array();

        $result = $db->query('SELECT avatar, text, date, name FROM review '
                . "LEFT JOIN user "
                . "ON review.user_id = user.id "
                . "WHERE status = '1' "
                . "LIMIT " . self::SHOW_BY_DEFAULT
                . ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {            
            $reviewsList[$i]['avatar'] = $row['avatar'];
            $reviewsList[$i]['text'] = $row['text'];
            $reviewsList[$i]['date'] = $row['date'];
            $reviewsList[$i]['name'] = $row['name'];

            $i++;
        }

        return $reviewsList;
    }

    // Возвращает количество отзывов
    public static function getTotalReviews() {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM review '
                . 'WHERE status="1" ');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();


        return $row['count'];
    }

    // Добавление нового отзыва
    public static function addNewReview($userId, $text) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO review (user_id, text) '
                . 'VALUES (:user_id, :text)';

        $result = $db->prepare($sql);        
        $result->bindParam(':user_id', $userId, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);

        return $result->execute();
    }

}
