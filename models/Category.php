<?php

class Category {

    // Возвращает общее количество новостей блога
    public static function getAllNews() {

        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM news '
                . 'WHERE status="1" ');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        $totalNews = $row['count'];

        return $totalNews;
    }

    // Возвращает массив с категориями новостей блога
    public static function getCategoriesList() {

        $db = Db::getConnection();

        $categoryList = array();

        $result = $db->query('SELECT id, name FROM category '
                . 'ORDER BY sort_order ASC');

        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $i++;
        }

        return $categoryList;
    }

    // Возвращает максимальный номер категории
    public static function getMaxValueInCategory() {
        $db = Db::getConnection();

        $result = $db->query('SELECT MAX(category_id) AS category_id FROM news');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        $max = $row['category_id'];

        return $max;
    }

    // Возвращает массив (категории + количество новостей по категориям)
    public static function getAddTotalNewsInCategoriesList() {
        $db = Db::getConnection();
        $max = self::getMaxValueInCategory();
        $totalInSidebar = self::getCategoriesList();

        $i = 0;
        for ($count = 2; $count <= $max; ++$count) {

            $result = $db->query('SELECT count(id) AS amount FROM news '
                    . 'WHERE status="1" AND category_id ="' . $count . '"');
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $row = $result->fetch();
            $totalInSidebar[$i]['amount'] = $row['amount'];
            $i++;
        }

        return $totalInSidebar;
    }

    // Возвращает массив с категориями портфолио
    public static function getPortfolioCategoriesList() {

        $db = Db::getConnection();

        $categoryList = array();

        $result = $db->query('SELECT id, name, filter FROM portfolio_category '
                . 'ORDER BY sort_order ASC');

        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $categoryList[$i]['filter'] = $row['filter'];
            
            $i++;
        }

        return $categoryList;
    }

}
