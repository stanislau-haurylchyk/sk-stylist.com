<?php

return array(
    'test' => 'test/test',
    'about-me' => 'introduce/view', // actionView в IntroduceController
    'services/prices' => 'service/index', // actionView в ServiceController
    'services' => 'service/view', // actionView в ServiceController
    'school' => 'school/view', // actionView в SchoolController
    'portfolio/page-([0-9]+)' => 'portfolio/index/$1', // actionIndex в PortfolioController    
    'blog/news-([0-9]+)' => 'blog/view/$1', // actionView в BlogController
    'blog/news/([0-9]+)/page-([0-9]+)' => 'blog/index/$1/$2', // actionIndex в BlogController
    'reviews/page-([0-9]+)' => 'review/index/$1', // actionIndex в ReviewController
    'contacts' => 'site/contact', // actionContact в SiteController
    'user/register' => 'user/register', // actionRegister в UserController
    'cabinet' => 'cabinet/index',
    'user/login/forgotpass' => 'user/forgotpass',
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    '.*.' => 'site/index', // Если страница не найдена, то редирект на главную страницу (404)
    '' => 'site/index' // actionIndex в SiteController    
);
