<?php

class BlogController {

    //Вывод главной страницы блога + последние 3 нововсти
    public function actionIndex($categoryId = 1, $page = 1) {

        $currentPage = $page;
        $lastPage = 1;

        $categories = array();
        $categories = Category::getAddTotalNewsInCategoriesList();

        $categoryNews = array();
        $categoryNews = Blog::getNewsListByCategory($categoryId, $page);

        $total = Blog::getTotalNewsInCategory($categoryId);
        $totalNews = Category::getAllNews();

        $lastPortfolioNews = array();
        $lastPortfolioNews = Portfolio::getLastPortfolioNews();

        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Blog::SHOW_BY_DEFAULT, 'page-');

        require_once(ROOT . '/views/blog/index.php');

        return true;
    }

    //Вывод одной новости блога
    public function actionView($id) {

        $categories = array();
        $categories = Category::getAddTotalNewsInCategoriesList();

        $total = Blog::getTotalNewsInCategory($categoryId = 3);
        $totalNews = Category::getAllNews();

        if ($id) {
            $newsItem = Blog::getNewsItemByID($id);

            require_once(ROOT . '/views/blog/view.php');
        }

        return true;
    }

}
