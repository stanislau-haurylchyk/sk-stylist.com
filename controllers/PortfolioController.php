<?php

class PortfolioController {

    public function actionIndex($page = 1) {
        
        $currentPage = $page;
        $lastPage = 1;
        
        $categoriesById = array();
        $categoriesById = Portfolio::getPortfolioCategoryNameById();
        
        $categories = array();
        $categories = Category::getPortfolioCategoriesList();
        
        $newsList = array();
        $newsList = Portfolio::getAllPortfolioNews($page);
        
        $photoUrl = array();
        $photoUrl = Portfolio::getPortfolioPhotoUrl();
        
        $total = Portfolio::getTotalNewsInPortfolio();
                        
        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Portfolio::SHOW_BY_DEFAULT, 'page-');

        require_once(ROOT . '/views/portfolio/index.php');

        return true;
    }

}
