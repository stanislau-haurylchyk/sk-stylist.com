<?php

class SiteController {

    public function actionIndex() {

        require_once(ROOT . '/views/main/index.php');

        return true;
    }

    public function actionContact() {

        $userName = '';
        $userEmail = '';
        $userPhone = '';
        $userMessage = '';


        $result = false;

        if (isset($_POST['submit'])) {

            $userEmail = $_POST['userEmail'];
            $userMessage = $_POST['userMessage'];
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];

            $errors = false;

            // Валидация полей
            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }

            if ($errors == false) {
                $adminEmail = 'stanislau.haurylchyk@gmail.com';
                $message = "Текст: {$userMessage}. От {$userEmail}. Имя: {$userName}. Номер телефона: {$userPhone}";
                $subject = 'sky-stylist - форма обратной связи';
                $result = mail($adminEmail, $subject, $message);
                $result = true;
            }
        }

        require_once(ROOT . '/views/contacts/index.php');

        return true;
    }

}
