<?php

class ServiceController {

    public function actionView() {
        require_once(ROOT . '/views/services/view.php');

        return true;
    }

    public function actionIndex() {
        require_once(ROOT . '/views/services/index.php');

        return true;
    }
}
