<?php

class ReviewController {

    public function actionIndex($page = 1) {

        $currentPage = $page;

        $reviewsList = array();
        $reviewsList = Review::getAllReviews($page);

        $total = Review::getTotalReviews();

        // Создаем объект Pagination - постраничная навигация
        $pagination = new Pagination($total, $page, Review::SHOW_BY_DEFAULT, 'page-');

        if (isset($_POST['submit'])) {
            $name = $_SESSION['user'];
            $text = htmlspecialchars($_POST['text']);
            Review::addNewReview($name, $text);
        }

        require_once(ROOT . '/views/review/view.php');

        return true;
    }

}
