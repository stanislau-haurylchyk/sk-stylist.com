<?php

$title = 'Услуги и цены | Снежана Королёва';
$page = 'services';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <div class="text-center">
                <h1 class="no-top-margin">Услуги и цены</h1>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <p>
                            Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari.
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <article class="profile-short">
                    <div class="profile-short-img">
                        <img alt="avatar" src="/template/images/avatar/1.jpg">
                        <div class="profile-short-job">Брови</div>
                    </div>
                    <div class="profile-short-info">
                        <a href="services/prices#brows" class="button-dark button-long">Посмотреть цену</a>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>                        
                    </div>
                </article>
                <article class="profile-short">
                    <div class="profile-short-img">
                        <img alt="avatar" src="/template/images/avatar/1.jpg">
                        <div class="profile-short-job">Ресницы</div>
                    </div>
                    <div class="profile-short-info">
                        <a href="services/prices#eyelashes" class="button-dark button-long">Подробнее...</a>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>                        
                    </div>
                </article>
                <article class="profile-short">
                    <div class="profile-short-img">
                        <img alt="avatar" src="/template/images/avatar/3.jpg">
                        <div class="profile-short-job">Макияж</div>
                    </div>
                    <div class="profile-short-info">
                        <a href="#" class="button-dark button-long">Подробнее...</a>
                        <h3></h3>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>                        
                    </div>
                </article>
                <article class="profile-short">
                    <div class="profile-short-img">
                        <img alt="avatar" src="/template/images/avatar/4.jpg">
                        <div class="profile-short-job">Прически</div>
                    </div>
                    <div class="profile-short-info">
                        <a href="#" class="button-dark button-long">Подробнее...</a>
                        <h3></h3>
                        <p>Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>                        
                    </div>
                </article>


                <article class="profile-short">
                    <div class="profile-short-img">
                        <img alt="avatar" src="/template/images/avatar/3.jpg">
                        <div class="profile-short-job">Курс "Макияж для себя"</div>
                    </div>
                    <div class="profile-short-info">
                        <a href="#" class="button-dark button-long">Подробнее...</a>
                        <h3></h3>
                        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>                        
                    </div>
                </article>
                <article class="profile-short">
                    <div class="profile-short-img">
                        <img alt="avatar" src="/template/images/avatar/4.jpg">
                        <div class="profile-short-job">Курс "Brow Maker"</div>
                    </div>
                    <div class="profile-short-info">
                        <a href="#" class="button-dark button-long">Подробнее...</a>
                        <h3></h3>
                        <p>Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>                        
                    </div>
                </article>
            </div><!-- .clearfix -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php

include ROOT . '\views\layouts\footer.php';
