<?php
$title = 'Кабинет пользователя | Снежана Королёва';
$page = 'cabinet';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <h1 class="no-top-margin">Ваш личный кабинет</h1>
            <div class="row row-big">
                <div class="col-xxl-7 content-column">
                    <p>Здравствуйте, <?php echo $user['name'];?>! Добро пожаловать в Ваш личный кабинет :)</p>                    
                </div><!-- .col-xxl-7 -->
                <div class="col-xxl-5 content-column">

                    <ul>
                        <li><a href="/user/edit">Мой профиль</a></li>
                        <li><a href="/user/history">Мои сообщения</a></li>
                    </ul>
                </div><!-- .col-xxl-5 -->
            </div><!-- .row -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php
include ROOT . '\views\layouts\footer.php';