<?php

$title = 'Контакты | Снежана Королёва';
$page = 'contacts';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <h1 class="no-top-margin">Отзывы клиентов</h1>
            <div class="comment">
                <div class="comment-container">
                    <div class="comment-avatar">
                        <img alt="avatar" src="/template/images/avatar/1_small.jpg">
                    </div>

                    <div class="comment-content">
                        <p class="comment-author">John Doe</p>
                        <span class="comment-detail">September 4, 2014 at 1:08 am</span>
                        <p>
                            Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                        </p>
                    </div>
                </div>
                <div class="comment">
                    <div class="comment-container">
                        <div class="comment-avatar">
                            <img alt="avatar" src="/template/images/avatar/2_small.jpg">
                        </div>

                        <div class="comment-content">
                            <p class="comment-author">Frank Bush</p>
                            <span class="comment-detail">September 4, 2014 at 1:08 am</span>
                            <p>
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comment">
                <div class="comment-container">
                    <div class="comment-avatar">
                        <img alt="avatar" src="/template/images/avatar/1_small.jpg">
                    </div>

                    <div class="comment-content">
                        <p class="comment-author">Britney Doe</p>
                        <span class="comment-detail">September 4, 2014 at 1:08 am</span>
                        <p>
                            Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="margin-10"></div>
            <h2>Оставить отзыв</h2>

            <form>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="input-container">
                            <input type="text" placeholder="Name">
                        </div>
                        <div class="input-container">
                            <input type="text" placeholder="Email">
                        </div>
                        <div class="input-container">
                            <input type="text" placeholder="Website">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-container">
                            <textarea placeholder="Message"></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <input class="pull-left submit-small" type="submit" value="Submit Comment">
                </div>
                <div class="margin-20"></div>
            </form>
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php

include ROOT . '\views\layouts\footer.php';
