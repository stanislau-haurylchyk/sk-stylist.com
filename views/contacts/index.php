<?php
$title = 'Контакты | Снежана Королёва';
$page = 'contacts';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <h1 class="no-top-margin">Мои контакты</h1>
            <div class="row row-big">
                <div class="col-xxl-7 content-column">
                    <p>123A, Molestie Lorem Avenue, Aliquam AAA0010</p>
                    <p>Tel: (+20) 21 301 524</p>
                    <p><a href="#">info@loremipsum.com</a></p>
                    <!--Соц. сети-->
                    <a href="https://www.instagram.com/sk_stylist_makeup/" target="_blank"><img alt="facebook" src="/template/images/share/instagram.png"></a>
                    <a href="#" target="_blank"><img alt="vk" src="/template/images/share/vk.png"></a>
                    <a href="#" target="_blank"><img alt="telegram" src="/template/images/share/telegram.png"></a>
                    <a href="https://www.facebook.com/snezhana.kovalyova" target="_blank"><img alt="facebook" src="/template/images/share/facebook.png"></a>
                    <div class="margin-30"></div>
                    <div class="map-container">
                        <div id="map-canvas">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2348.646341524755!2d27.45189971616064!3d53.938026680107285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dbc4ef56210839%3A0xb09784250e1dedb!2z0YPQuy4g0JrQsNC80LDQudGB0LrQsNGPIDIwLCDQnNC40L3RgdC6!5e0!3m2!1sru!2sby!4v1531430694235" width="550" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div><!-- .col-xxl-7 -->
                <div class="col-xxl-5 content-column">
                    <h2 class="no-top-margin">Обратная связь</h2>
                    <!--Проверка полей ввода на ошибки-->
                    <?php if ($result): ?>
                        <p>Сообщение отправлено! Я свяжусь с Вами по указанному E-mail или телефону.</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <!--Форма обратной связи-->
                        <form id="form-contact2" action="#" method="post" data-email-not-set-msg="Email is required" data-message-not-set-msg="Message is required" data-ajax-fail-msg="Request could not be sent, try later" data-success-msg="Email successfully sent.">
                            <input type="text" name="userName" placeholder="Имя" value="<?php echo $userName; ?>">
                            <input type="email" name="userEmail" placeholder="E-mail" value="<?php echo $userEmail; ?>">
                            <input type="text" name="userPhone" placeholder="Номер телефона" value="<?php echo $userPhone; ?>">
                            <textarea name="userMessage" placeholder="Сообщение" value="<?php echo $userMessage; ?>"></textarea>
                            <p class="return-msg"></p>
                            <div class="margin-10"></div>
                            <div class="clearfix">
                                <input class="submit-small pull-left" type="submit" name="submit" value="Отправить сообщение">
                                <p class="note">
                                    Есть вопрос? Напишите мне :)
                                </p>                                
                            </div>
                        </form>
                    <?php endif; ?>
                </div><!-- .col-xxl-5 -->
            </div><!-- .row -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php
include ROOT . '\views\layouts\footer.php';
