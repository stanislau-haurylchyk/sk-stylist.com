<?php
$title = 'Регистрация | Снежана Королёва';
$page = 'register';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <h1 class="no-top-margin">Регистрация на сайте</h1>
            <div class="row row-big">
                <div class="col-xxl-7 content-column">                    
                    <p>Пройдите простую процедуру регистрации, чтобы получить доступ ко всем возможностям сайта :)</p>
                    <!--Блок вывода ошибок-->
                    <div class="errors">
                    <?php if ($result): ?>
                        <p>Поздравляю! Вы успешно зарегистрированы :)</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        </div>
                        <!--Конц блока вывода ошибок-->
                    </div><!-- .col-xxl-7 -->
                    <div class="col-xxl-5 content-column">
                        <form action="#" method="post">
                            <input type="text" name="name" placeholder="Имя" value="<?php echo $name; ?>"/>
                            <input type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                            <input type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>                            
                            <input type="submit" name="submit" class="submit-small pull-left" value="Регистрация" />                                                                                    
                        </form>
                    <?php endif; ?>
                </div><!-- .col-xxl-5 -->

            </div><!-- .row -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php
include ROOT . '\views\layouts\footer.php';
