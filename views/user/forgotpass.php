<?php
$title = 'Восстановление пароля | Снежана Королёва';
$page = 'login';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <h1 class="no-top-margin">Восстановление пароля</h1>
            <div class="row row-big">
                <div class="col-xxl-7 content-column">
                    <p>Введите e-mail, который был указан при регистрации и мы вышлем на него Ваш пароль :)</p>
                    <!--Блок вывода ошибок-->
                    <div class="errors">
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <!--Конц блока вывода ошибок-->
                </div><!-- .col-xxl-7 -->
                <div class="col-xxl-5 content-column">
                    <form action="#" method="post">                        
                        <input type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                        <input type="submit" name="submit" class="submit-small pull-left" value="Выслать пароль" />
                    </form>                    
                </div><!-- .col-xxl-5 -->
            </div><!-- .row -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php
include ROOT . '\views\layouts\footer.php';
