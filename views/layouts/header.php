<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" xml:lang="en-gb" class="no-js"> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <!--[if lt IE 9]> 
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="jThemes Studio">
        <meta name="robots" content="index, follow">
        <meta name="viewport" content="width=device-width, initial-scale=1">    

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/template/ico/apple-touch-icon-144-precomposed.png">
        <link rel="shortcut icon" href="/template/ico/favicon.ico">

        <!--[if lt IE 9]>
            <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,900,900italic,700italic,400italic%7CFjalla+One' rel='stylesheet' type='text/css'>

        <!-- Icon-Font -->
        <link rel="stylesheet" href="/template/font-awesome/font-awesome/css/font-awesome.min.css" type="text/css">
        <!--[if IE 7]>
            <link rel="stylesheet" href="/template/font-awesome/font-awesome/css/font-awesome-ie7.min.css" type="text/css">
        <![endif]-->

        <link rel="stylesheet" href="/template/bootstrap/css/bootstrap.min.css" type="text/css">
        <?php require_once(ROOT . $styles); ?>
        <link rel="stylesheet" href="/template/styles/main.css" type="text/css">
        <link rel="stylesheet" href="/template/styles/white.css" type="text/css">
        <link rel="stylesheet" href="/template/leaguegothic/leaguegothic.css" type="text/css">

        <title><?php echo $title; ?></title>

        <script type="text/javascript" src="/template/js/modernizr.min.js"></script>
        <script type="text/javascript" src="/template/pace/pace.min.js"></script>
    </head>

    <body>
        <div id="page-loader">
            <div class="loader-square loader-square-1">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-2">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-3">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-4">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-5">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-6">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-7">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-8">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-square loader-square-9">
                <div class="loader-square-content">
                    <div class="loader-square-inner bg-pattern white-screen"></div>
                </div>
            </div>
            <div class="loader-container">
                <div class="loader-content">
                    <img class="loader-logo" alt="Smarty" src="/template/images/logo2.png">
                </div>
            </div><!-- .loader-container -->
            <div class="loader-footer">
                <p>
                    Copyright &copy; <?php echo date("Y");?> <span class="text-white">sky-stylist</span>.<br>All
                    Rights Reserved.
                </p>
            </div>
        </div><!-- #page-loader -->

        <div id="all">
            <div id="menu">
                <div class="menu-container">
                    <div class="menu-inner">
                        <div class="logo-container">
                            <img alt="sk-stylist" src="/template/images/logo.png">
                        </div>
                        <nav class="main-navigation">
                            <?php include ROOT . '\views\layouts\navigation.php'; ?>
                        </nav>
                        <!--Поиск-->
                        <div class="menu-content">
                            <form id="form-search">
                                <input type="text" placeholder="Поиск...">
                                <div class="submit-container">
                                    <input type="submit" value="Send">
                                </div>
                            </form>
                        </div>
                        <!--Вход на сайт и регистрация-->
                        <div class="text-center">
                            <?php if (User::isGuest()): ?>
                            <a href="/user/register"><i class="fa fa-user"></i> Регистрация</a><br>
                            <a href="/user/login"><i class="fa fa-lock"></i> Вход</a><br>
                            <?php else: ?>
                            <a href="/cabinet"><i class="fa fa-user"></i> Кабинет</a><br>
                            <a href="/user/logout/"><i class="fa fa-unlock"></i> Выход</a><br>                            
                            <?php endif; ?>
                        </div>
                        <!--Ссылки на соцсети-->
                        <div class="menu-bottom">
                            <div class="text-center">
                                <a href="#"><i class="fa fa-facebook-square"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                            <div class="margin-10"></div>
                            <p>Copyright &copy; 2018 <span class="text-highlight">sky-stylist</span>. All Rights Reserved.</p>
                        </div>
                    </div><!-- .menu-inner -->
                </div><!-- .menu-container -->
            </div><!-- #menu -->
            <div id="menu-trigger">
                <i class="fa fa-reorder"></i>
            </div>