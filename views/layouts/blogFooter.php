</div><!-- .content-main -->
            
            <!--Правый сайдбар-->
            <div class="content-sidebar">
                <!--Категории-->
                <h4 class="sidebar-heading">Категории</h4>
                <ul class="list-catgs">
                    <li>
                        <a href="/blog/news/1/page-1">
                            <span class="list-catg-name">Все</span>                            
                            <span class="list-catg-val"><?php echo $totalNews; ?></span>
                        </a>             
                    </li>
                    <?php foreach ($categories as $categoryItem): ?>
                    <li>
                        <a href="/blog/news/<?php echo $categoryItem['id'];?>/page-1">
                            <span class="list-catg-name"><?php echo $categoryItem['name'];?></span>                            
                            <span class="list-catg-val"><?php echo $categoryItem['amount'];?></span>
                        </a>                        
                    </li>
                    <?php endforeach; ?>                    
                </ul>
                
                <!--Популярные посты-->
                <h4 class="sidebar-heading">Популярные посты</h4>
                <a href="#" class="post-small-container">
                    <article class="post-small">
                        <div class="post-small-img">
                            <img alt="post thumbnail" src="/template/images/blog/thumbnail1.png">
                        </div>
                        <p class="post-small-content">Duis autem vel eum iriure hendrerit in vulputate...</p>
                    </article>
                </a>
                <a href="#" class="post-small-container">
                    <article class="post-small">
                        <div class="post-small-img">
                            <img alt="post thumbnail" src="/template/images/blog/thumbnail2.png">
                        </div>
                        <p class="post-small-content">Lorem ipsum dolor sit amet, consectetuer...</p>
                    </article>
                </a>
                <a href="#" class="post-small-container">
                    <article class="post-small">
                        <div class="post-small-img">
                            <img alt="post thumbnail" src="/template/images/blog/thumbnail3.png">
                        </div>
                        <p class="post-small-content">Claritas est etiam processus dynamicus sequitur...</p>
                    </article>
                </a>
                
                <!--Читаем-->
                <h4 class="sidebar-heading">Цитаты</h4>
                <div class="sidebar-section">
                    <div class="margin-10"></div>
                    <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes.</p>
                    <a href="#" class="link-read-more">Read More</a>
                    <div class="margin-10"></div>
                </div>
                
                <!--Смотрим-->
                <h4 class="sidebar-heading">Галерея</h4>
                <div class="margin-20"></div>
                <div class="sidebar-section">
                    <div class="row row-small">
                        
                        <!--Выводим последние новости из галереи-->
                        <?php foreach ($lastPortfolioNews as $portfolioItem):?>
                        <div class="col-xs-4">
                            <a class="link-borders" href="/portfolio/page-1"><img class="img-full" alt="img" src="/template/images/portfolio_grid/<?php echo $portfolioItem['preview']; ?>.jpg"></a>
                        </div>
                        <?php endforeach;?>                        
                    </div>
                </div>
                
                <!--Отзывы-->
                <h4 class="sidebar-heading">Отзывы</h4>
                <div class="single-slider-v2 top-small-arrows">
                    <div class="testimonial-small">
                        <div class="testimonial-small-img"><img alt="author" src="/template/images/profiles/small1.png"></div>
                        <p>
                            “Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes.”
                        </p>
                        <p class="testimonial-small-detail">
                            <strong>John Doe</strong><br>
                            (Name Company)
                        </p>
                    </div>
                    <div class="testimonial-small">
                        <div class="testimonial-small-img"><img alt="author" src="/template/images/profiles/small1.png"></div>
                        <p>
                            “Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes.”
                        </p>
                        <p class="testimonial-small-detail">
                            <strong>John Doe</strong><br>
                            (Name Company)
                        </p>
                    </div>
                </div>
            </div><!-- .content-sidebar -->
        </div><!-- .container -->
    </div><!-- .section-content -->
</section>