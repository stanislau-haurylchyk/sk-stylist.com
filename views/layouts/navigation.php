<ul>
    <li <?php echo ($page == 'main') ? "class='active'" : ""; ?> >
        <a href="/">Главная</a>
    </li>
    <li <?php echo ($page == 'about-me') ? "class='active'" : ""; ?> >
        <a href="/about-me">Обо мне</a>
    </li>
    <li <?php echo ($page == 'services') ? "class='active'" : ""; ?> >
        <a href="/services">Услуги и цены</a>
    </li>
    <li <?php echo ($page == 'school') ? "class='active'" : ""; ?> >
        <a href="/school">Обучение</a>
    </li>
    <li <?php echo ($page == 'portfolio') ? "class='active'" : ""; ?> >
        <a href="/portfolio/page-1">Портфолио</a>
    </li>
    <li <?php echo ($page == 'blog') ? "class='active'" : ""; ?> >
        <a href="/blog/news/1/page-1">Блог</a>
    </li>
    <li <?php echo ($page == 'reviews') ? "class='active'" : ""; ?> >
        <a href="/reviews/page-1">Отзывы</a>
    </li>
    <li <?php echo ($page == 'contacts') ? "class='active'" : ""; ?> >
        <a href="/contacts">Контакты</a>
    </li>
</ul>