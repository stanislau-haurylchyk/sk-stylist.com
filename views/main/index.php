<?php
$title = 'sky-stylist';
$page = 'main';
$styles = '\views\layouts\mainPageCSSheader.php';
$scripts = '\views\layouts\mainPageJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>        

<div class="ms-fullscreen-template" id="slider1-wrapper">
    <!-- masterslider -->
    <div class="master-slider ms-skin-default has-thumbnails" id="masterslider">
        <div class="ms-slide ms-slide1" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen-2"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/1.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size3 text-uppercase" data-type="text" data-effect="rotate3dtop(70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <h2>Welcome! This is Smarty</h2>
                </div>
            </div>
            <div class="ms-layer ms-layer2 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <p>
                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie<br>
                        consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.
                    </p>
                </div>
            </div>
            <div class="ms-layer ms-layer3 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-delay="300" data-ease="easeInOutQuint">
                <div class="container">
                    <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/1.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide2 text-right" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen-3"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/2.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size1" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-ease="easeInOutQuint">
                <div class="container">
                    <h2 class="text-highlight text-uppercase">Sale</h2>
                </div>
            </div>
            <div class="ms-layer ms-layer2 ms-text-size5" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-delay="300" data-ease="easeInOutQuint">
                <div class="container text-right">
                    <h2 class="text-uppercase">
                        Duis autem vel eum iriure<br>
                        Lorem ipsum dolor
                    </h2>
                </div>
            </div>
            <div class="ms-layer ms-layer3 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-delay="600" data-ease="easeInOutQuint">
                <div class="container">
                    <p>
                        Duis autem vel eum iriure dolor in hendrerit in vulputate<br>
                        velit esse molestie consequat, vel illum dolore eu.
                    </p>
                </div>
            </div>
            <div class="ms-layer ms-layer4 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-delay="900" data-ease="easeInOutQuint">
                <div class="container">
                    <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/2.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide3 text-right" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/3.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size2" data-type="text" data-effect="rotateleft(5,long,br,true)" data-duration="1500" data-ease="easeInOutQuint">
                <div class="container">
                    <div class="row">
                        <div class="col-xxl-6">
                            <h2 class="text-uppercase text-highlight">For Women</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-layer ms-layer2 ms-text-size5" data-type="text" data-effect="bottom(70, true)" data-duration="2000" data-delay="1000" data-ease="easeInOutQuint">
                <div class="container text-uppercase">
                    <div class="row">
                        <div class="col-xxl-6">
                            <h3>
                                Duis autem vel eum iriure<br>
                                Lorem ipsum dolor
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-layer ms-layer3 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="bottom(50, true)" data-duration="2000" data-delay="1400" data-ease="easeInOutQuint">
                <div class="container">
                    <div class="row">
                        <div class="col-xxl-6">
                            <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/3.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide4" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen-5"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/4.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size2 text-uppercase" data-type="text" data-effect="rotate3dtop(70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <h2>Sexy Dresses</h2>
                </div>
            </div>
            <div class="ms-layer ms-layer2 ms-text-size4 text-uppercase" data-type="text" data-effect="rotate3dright(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <h3>Claritas processus</h3>
                </div>
            </div>
            <div class="ms-layer ms-layer3 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-delay="300" data-ease="easeInOutQuint">
                <div class="container">
                    <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/4.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide1" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen-2"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/1.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size3 text-uppercase" data-type="text" data-effect="rotate3dtop(70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <h2>Welcome! This is Smarty</h2>
                </div>
            </div>
            <div class="ms-layer ms-layer2 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <p>
                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie<br>
                        consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.
                    </p>
                </div>
            </div>
            <div class="ms-layer ms-layer3 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-delay="300" data-ease="easeInOutQuint">
                <div class="container">
                    <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/5.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide2 text-right" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen-3"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/2.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size1" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-ease="easeInOutQuint">
                <div class="container">
                    <h2 class="text-highlight text-uppercase">Sale</h2>
                </div>
            </div>
            <div class="ms-layer ms-layer2 ms-text-size5" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-delay="300" data-ease="easeInOutQuint">
                <div class="container text-right">
                    <h2 class="text-uppercase">
                        Duis autem vel eum iriure<br>
                        Lorem ipsum dolor
                    </h2>
                </div>
            </div>
            <div class="ms-layer ms-layer3 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-delay="600" data-ease="easeInOutQuint">
                <div class="container">
                    <p>
                        Duis autem vel eum iriure dolor in hendrerit in vulputate<br>
                        velit esse molestie consequat, vel illum dolore eu.
                    </p>
                </div>
            </div>
            <div class="ms-layer ms-layer4 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="skewleft(10,100,true)" data-duration="1500" data-delay="900" data-ease="easeInOutQuint">
                <div class="container">
                    <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/6.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide3 text-right" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/3.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size2" data-type="text" data-effect="rotateleft(5,long,br,true)" data-duration="1500" data-ease="easeInOutQuint">
                <div class="container">
                    <div class="row">
                        <div class="col-xxl-6">
                            <h2 class="text-uppercase text-highlight">For Women</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-layer ms-layer2 ms-text-size5" data-type="text" data-effect="bottom(70, true)" data-duration="2000" data-delay="1000" data-ease="easeInOutQuint">
                <div class="container text-uppercase">
                    <div class="row">
                        <div class="col-xxl-6">
                            <h3>
                                Duis autem vel eum iriure<br>
                                Lorem ipsum dolor
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-layer ms-layer3 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="bottom(50, true)" data-duration="2000" data-delay="1400" data-ease="easeInOutQuint">
                <div class="container">
                    <div class="row">
                        <div class="col-xxl-6">
                            <a class="button" href="#">Подробнее <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/7.jpg"></div>
        </div><!-- .ms-slide -->
        <div class="ms-slide ms-slide4" data-delay="7">
            <div class="ms-slide-pattern bg-pattern white-screen-5"></div>
            <img src="template/masterslider/blank.gif" data-src="template/images/slider/4.jpg" alt="lorem ipsum dolor sit">   
            <div class="ms-layer ms-layer1 ms-text-size2 text-uppercase" data-type="text" data-effect="rotate3dtop(70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <h2>Sexy Dresses</h2>
                </div>
            </div>
            <div class="ms-layer ms-layer2 ms-text-size4 text-uppercase" data-type="text" data-effect="rotate3dright(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                <div class="container">
                    <h3>Claritas processus</h3>
                </div>
            </div>
            <div class="ms-layer ms-layer3 ms-text-size7 visible-xxxl visible-xxl visible-xl visible-lg visible-md" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-delay="300" data-ease="easeInOutQuint">
                <div class="container">
                    <a class="button" href="#">Learn More <i class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <div class="ms-thumb"><img alt="img" src="template/images/slider_thumbs/8.jpg"></div>
        </div><!-- .ms-slide -->
    </div>
    <!-- end of masterslider -->
</div><!-- .ms-fullscreen-template -->

<?php
include ROOT . '\views\layouts\footer.php';