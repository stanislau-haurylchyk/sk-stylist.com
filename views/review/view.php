<?php
$title = 'Отзывы | Снежана Королёва';
$page = 'reviews';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="section-page container">
            <h1 class="no-top-margin">Отзывы клиентов</h1>

            <?php
            for ($i = 0; $i < count($reviewsList); $i++):
                ?>                
                <div class="comment">                
                    <div class="comment-container">
                        <div class="comment-avatar">
                            <img alt="avatar" src="/template/images/avatar/<?php echo $reviewsList[$i]['avatar']; ?>.jpg">
                        </div>

                        <div class="comment-content">
                            <p class="comment-author"><?php echo $reviewsList[$i]['name']; ?></p>
                            <span class="comment-detail"><?php echo $reviewsList[$i]['date']; ?></span>
                            <p>
                                <?php echo $reviewsList[$i]['text']; ?>
                            </p>
                        </div>
                    </div>

                    <?php
                    $i++;
                    if ($i >= count($reviewsList)) {
                        break;
                    }
                    ?>

                    <div class="comment">
                        <div class="comment-container">
                            <div class="comment-avatar">
                                <img alt="avatar" src="/template/images/avatar/<?php echo $reviewsList[$i]['avatar']; ?>.jpg">
                            </div>
                            <div class="comment-content">
                                <p class="comment-author"><?php echo $reviewsList[$i]['name']; ?></p>
                                <span class="comment-detail"><?php echo $reviewsList[$i]['date']; ?></span>
                                <p>
                                    <?php echo $reviewsList[$i]['text']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                $i++;
                if ($i >= count($reviewsList)) {
                    break;
                }
                ?>

                <div class="comment">
                    <div class="comment-container">
                        <div class="comment-avatar">
                            <img alt="avatar" src="/template/images/avatar/<?php echo $reviewsList[$i]['avatar']; ?>.jpg">
                        </div>

                        <div class="comment-content">
                            <p class="comment-author"><?php echo $reviewsList[$i]['name']; ?></p>
                            <span class="comment-detail"><?php echo $reviewsList[$i]['date']; ?></span>
                            <p>
                                <?php echo $reviewsList[$i]['text']; ?> 
                            </p>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>

            <!-- Постраничная навигация -->
            <div class="pagination-short">
                <?php echo $pagination->get(); ?>                
                <div class="pagination-overview">
                    Страница <span class="pagination-number-active"><?php echo $currentPage; ?></span> из <span class="pagination-number-total"><?php echo $pagination->amount(); ?></span>
                </div>
            </div>
            <!-- Конец постраничной навигации -->
            <div class="margin-10"></div>
            
            <!-- Оставить отзыв -->
            <?php if (User::isGuest()): ?>
                <h4>Оставлять отзывы могут только зарегистрированные пользователи</h4>
                <div class="clearfix">
                    <a href="/user/register" class="button-big">Регистрация</a>
                </div>
            <?php else: ?>
                <h4>Написать отзыв</h4>
                <?php var_dump($_SESSION);?>
                <form action="#" method="post">
                    <div class="row">                        
                        <div class="col-sm-6">
                            <div class="input-container">
                                <textarea name="text" placeholder="Введите текст"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <input class="pull-left submit-small" type="submit" name="submit" value="Оставить отзыв">
                    </div>
                    <div class="margin-20"></div>
                </form>
            <?php endif; ?>
            <!-- Конец блока "Оставить отзыв" -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php
include ROOT . '\views\layouts\footer.php';
