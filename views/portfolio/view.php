<?php
$c = 0;
$n = 1;
$curr = $photoUrl[$c]['id'];
$next = $photoUrl[$n]['id'];
foreach ($newsList as $newsItem):
    ?>
    <section id="popup-portfolio-<?php echo $newsItem['id']; ?>" class="popup-window-container">
        <div class="section-content">
            <div class="popup-window-closing-area"></div>
            <div class="container">
                <div class="popup-window portfolio-work">
                    <div class="popup-window-close popup-window-close-light popup-window-close-small"></div>
                    <div class="portfolio-work-img">
                        <div class="single-slider-v2 black-arrows">

                            <!--Выводим фото, которые соответствуют своей новости в портфолио-->
                            <?php
                            foreach ($photoUrl as $url):
                                if ($curr == $next):
                                    ?>
                                    <a href = "/template/images/portfolio/<?php echo $photoUrl[$c]['photo_url']; ?>.jpg" data-lightbox = "portfolio-item1-images">
                                        <img alt = "image" src = "/template/images/portfolio/<?php echo $photoUrl[$c]['photo_url']; ?>.jpg">
                                    </a>
                                    <?php
                                    $c++;
                                    $n++;
                                    if ($n > (count($photoUrl) - 1)):
                                        $n = (count($photoUrl) - 1);
                                        ?>
                                        <a href = "/template/images/portfolio/<?php echo $photoUrl[$n]['photo_url']; ?>.jpg" data-lightbox = "portfolio-item1-images">
                                            <img alt = "image" src = "/template/images/portfolio/<?php echo $photoUrl[$n]['photo_url']; ?>.jpg">
                                        </a>
                                        <?php
                                        break;
                                    endif;

                                    $curr = $photoUrl[$c]['id'];
                                    $next = $photoUrl[$n]['id'];
                                else:
                                    ?>
                                    <a href = "/template/images/portfolio/<?php echo $photoUrl[$c]['photo_url']; ?>.jpg" data-lightbox = "portfolio-item1-images">
                                        <img alt = "image" src = "/template/images/portfolio/<?php echo $photoUrl[$c]['photo_url']; ?>.jpg">
                                    </a>
                                    <?php
                                    if ((count($photoUrl) - 1) > $n):
                                        $c++;
                                        $n++;
                                        $curr = $photoUrl[$c]['id'];
                                        $next = $photoUrl[$n]['id'];
                                    endif;
                                    break;
                                endif;
                            endforeach;
                            ?>
                            <!--Конец вывода фото-->





                        </div>
                    </div><!-- .portfolio-work-img -->
                    <div class="portfolio-work-detail">
                        <h3 class="no-top-margin"><?php echo $newsItem['title']; ?></h3>
                        <p>
                            <?php echo $newsItem['content']; ?>
                        </p>                    
                        <div class="margin-20"></div>
                        <a class="button" href="/contacts">Записаться на прием</a>
                        <div class="margin-30"></div>
                        <div class="links-box">
                            <span class="links-box-text">Поделиться в соц. сетях:</span>
                            <a href="https://www.instagram.com" target="_blank"><img alt="instagram" src="/template/images/share/instagram.png"></a>
                            <a href="https://www.vk.com" target="_blank"><img alt="vk" src="/template/images/share/vk.png"></a>
                            <a href="#" target="_blank"><img alt="telegram" src="/template/images/share/telegram.png"></a>
                            <a href="https://www.facebook.com/" target="_blank"><img alt="facebook" src="/template/images/share/facebook.png"></a>
                        </div>
                    </div>
                    <div class="portfolio-work-nav">
                        <a class="popup-window-close-trigger"><i class="fa fa-th"></i></a>
                        <a class="popup-window-prev"><i class="fa fa-2x fa-angle-left"></i></a>
                        <a class="popup-window-next"><i class="fa fa-2x fa-angle-right"></i></a>
                    </div><!-- .portfolio-work-detail -->
                </div><!-- .popup-window -->
            </div><!-- .container -->
        </div><!-- .section-content -->
    </section><!-- #popup-portfolio-№ -->
<?php endforeach; ?>