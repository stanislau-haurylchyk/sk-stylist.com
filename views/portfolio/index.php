<?php
$title = 'Портфолио | Снежана Королёва';
$page = 'portfolio';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\portfolioPageJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern dark-screen">
        <div class="container">            
            <div class="clearfix">
                <h1 class="page-top-heading pull-left">Портфолио</h1>
                <ul id="isotope-filter-list3" class="pull-right filter-list-alt">                    
                    <li><a class="active isotope-filter" data-filter="*" href="#">Все</a></li>

                    <!--Выводим категории портфолио-->
                    <?php foreach ($categories as $categoryItem): ?>                    
                        <li><a class="isotope-filter" data-filter=".filter-<?php echo $categoryItem['filter']; ?>" href="#"><?php echo $categoryItem['name']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <h5 class="page-top-heading pull-right">Фильтр по странице <?php echo $currentPage; ?>:</h5>


            <div id="portfolio3" class="portfolio-layout7 row row-clean">
                <?php $i = 0; ?>
                <?php foreach ($newsList as $newsItem): ?>
                    <article class="col-xxxl-4 col-xxl-4 col-xl-6 col-lg-12 col-md-6 col-sm-12 col-xs-12 portfolio-item filter-<?php echo $categoriesById[$i]['filter']; ?>">
                        <div class="portfolio-item-content popup-window-trigger" data-popup="#popup-portfolio-<?php echo $newsItem['id']; ?>">
                            <div class="portfolio-img">
                                <img alt="image" src="/template/images/portfolio_grid/<?php echo $newsItem['preview']; ?>.jpg">
                                <div class="portfolio-img-detail">
                                    <div class="portfolio-img-detail-inner">
                                        <div class="portfolio-img-detail-content">
                                            <div class="portfolio-item-heading">
                                                <div class="portfolio-item-cat">
                                                    <?php
                                                    echo $categoriesById[$i]['name'];
                                                    $i++;
                                                    ?>
                                                </div>
                                                <?php echo $newsItem['title']; ?>
                                            </div>
                                            <p class="portfolio-img-detail-hidden">
                                                <?php echo $newsItem['short_content']; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php endforeach; ?>               
            </div><!-- #portfolio3 -->

            <!-- Постраничная навигация -->
            <div class="pagination-short">
                <?php echo $pagination->get(); ?>                
                <div class="pagination-overview">
                    Страница <span class="pagination-number-active"><?php echo $currentPage; ?></span> из <span class="pagination-number-total"><?php echo $pagination->amount(); ?></span>
                </div>
            </div>
            <!-- Конец постраничной навигации -->


        </div><!-- .container -->
    </div><!-- .section-content -->
</section>


<?php include ROOT . '\views\portfolio\view.php'; ?>

<div id="page-screen-cover"></div>

<?php
include ROOT . '\views\layouts\footer.php';
