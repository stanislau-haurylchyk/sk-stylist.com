<?php
$title = 'Статья блога | Снежана Королёва';
$page = 'blog';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
include ROOT . '\views\layouts\blogHeader.php';
?>
<h1 class = "page-top-heading"><?php echo $newsItem['title'];?></h1>
<div class = "blog-layout-single">
    <article class = "blog-item">
        <div class = "blog-img">
            <div class = "blog-date">
                <div class = "blog-date-day"><?php $string = $newsItem['date'];
                                                                    $pattern = '/([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/';
                                                                    $replacement = '$3';
                                                                    echo preg_replace($pattern, $replacement, $string).'<br>';?></div>
                <?php echo $newsItem['date'];?>                
            </div>
            <div class = "single-slider-v2 black-arrows-bottom">
                <a href = "/template/images/blog/<?php echo $newsItem['preview'] . '.jpg';?>" data-lightbox = "blog-post"><img alt = "blog image" src = "/template/images/blog/<?php echo $newsItem['preview'] . '.jpg';?>"></a>
                <a href = "/template/images/blog/<?php echo $newsItem['preview'] . '.jpg';?>" data-lightbox = "blog-post"><img alt = "blog image" src = "/template/images/blog/<?php echo $newsItem['preview'] . '.jpg';?>"></a>
            </div>
        </div>
        <div class = "blog-content">
            <h4><?php echo $newsItem['short_content'];?></h4>
            <div class = "blog-details">Автор: <a href = "#"><?php echo $newsItem['author_name'];?></a> <span class = "delimiter-inline-alt"></span> В: <a href = "#">Photo</a>, <a href = "#">Photography</a> <span class = "delimiter-inline-alt"></span> Комментарии: <a href = "#">25</a> <span class = "delimiter-inline-alt"></span> Теги: <a href = "#">Photo</a>, <a href = "#">Photography</a>, <a href = "#">Photographer</a>, <a href = "#">Gallery</a></div>            
            <p><?php echo $newsItem['content'];?></p>
        </div>
    </article>
</div><!--.blog-layout-single -->

<?php
include ROOT . '\views\layouts\blogFooter.php';
include ROOT . '\views\layouts\footer.php';