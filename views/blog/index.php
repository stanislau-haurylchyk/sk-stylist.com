<?php
$title = 'Блог | Снежана Королёва';
$page = 'blog';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
include ROOT . '\views\layouts\blogHeader.php';
?>                
                <h1 class="page-top-heading">Мой блог</h1>
                <div class="blog-layout-big">
                    
                    <?php foreach ($categoryNews as $newsItem):?>
                    <!--Вид №1 отображения новости блога - для ТЕКСТА-->
                    <article class="blog-item centered-columns">
                        <div class="blog-img blog-img-hover centered-column" style="background-image:url('/template/images/blog/big.jpg');">
                            <div class="blog-date">
                                <div class="blog-date-day"><?php echo Date::getDay($newsItem['date']) . '<br>'; echo Date::getMonth($newsItem['date']); ?></div>
                                <?php echo $newsItem['date'];?>
                            </div>
                            <img alt="blog image" src="/template/images/blog/<?php echo $newsItem['preview'] . '.jpg';?>">
                            <div class="blog-img-detail">
                                <div class="blog-img-detail-inner">
                                    <div class="blog-img-detail-content">
                                        <a class="icon-circle" href="/template/images/blog/<?php echo $newsItem['preview'] . '.jpg';?>" data-lightbox="blog-post1"><i class="fa fa-search"></i></a>
                                        <a class="icon-circle" href="/blog/news-<?php echo $newsItem['id'];?>"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog-content centered-column centered-column-top">
                            <h4><a href="/blog/news-<?php echo $newsItem['id'];?>"><?php echo $newsItem['title'];?></a></h4>
                            <div class="blog-details">Автор: <a href="#"><?php echo $newsItem['author_name'];?></a> <span class="delimiter-inline-alt"></span> В: <a href="#">Photo</a>, <a href="#">Photography</a> <span class="delimiter-inline-alt"></span> Комментарии: <a href="#">25</a></div>
                            <p><?php echo $newsItem['short_content'];?></p>
                            <a class="button-border-dark" href="/blog/news-<?php echo $newsItem['id'];?>">Подробнее</a>
                        </div>
                    </article>                    
                    <?php endforeach;?>
                    
                    <!--Вид №2 отображения новости блога - для ВИДЕО
                    <article class="blog-item centered-columns">
                        <div class="blog-img centered-column">
                            <div class="blog-date">
                                <div class="blog-date-day">19</div>
                                September
                            </div>
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/rEk0AdJx9gw"></iframe>
                            </div>
                        </div>
                        <div class="blog-content centered-column centered-column-top">
                            <h4><a href="blogpost.html">Nam liber tempor cum soluta nobis eleifend option congue</a></h4>
                            <div class="blog-details">By <a href="#">John Doe</a> <span class="delimiter-inline-alt"></span> In <a href="#">Video</a> <span class="delimiter-inline-alt"></span> Comments: <a href="#">10</a></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            <a class="button-border-dark" href="blogpost.html">Read More</a>
                        </div>
                    </article>
                    
                    Вид №3 отображения новости блога - для ФОТО
                    <article class="blog-item centered-columns">
                        <div class="blog-img centered-column">
                            <div class="blog-date">
                                <div class="blog-date-day">08</div>
                                August
                            </div>
                            <div class="single-slider-v2 black-arrows-bottom">
                                <a href="/template/images/blog/big2.jpg" data-lightbox="blog-post3"><img alt="blog image" src="/template/images/blog/big2.jpg"></a>
                                <a href="/template/images/blog/big.jpg" data-lightbox="blog-post3"><img alt="blog image" src="/template/images/blog/big.jpg"></a>
                            </div>
                        </div>
                        <div class="blog-content centered-column centered-column-top">
                            <h4><a href="blogpost.html">Mirum est notare quam littera gothica, quam nunc putamus parum</a></h4>
                            <div class="blog-details">By <a href="#">John Doe</a> <span class="delimiter-inline-alt"></span> In <a href="#">Photo</a>, <a href="#">Photography</a>, <a href="#">Gallery</a> <span class="delimiter-inline-alt"></span> Comments: <a href="#">0</a></div>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            <a class="button-border-dark" href="blogpost.html">Read More</a>
                        </div>
                    </article>-->
                </div><!-- .blog-layout-big
                
                <!--Постраничная навигация-->
                <div class="pagination-short">
                    <?php echo $pagination->get(); ?>                    
                    <div class="pagination-overview">
                        Страница <span class="pagination-number-active"><?php echo $currentPage; ?></span> из <span class="pagination-number-total"><?php echo $pagination->amount(); ?></span>
                    </div>
                </div>
                <!-- Конец постраничной навигации -->
                
<?php
include ROOT . '\views\layouts\blogFooter.php';
include ROOT . '\views\layouts\footer.php';