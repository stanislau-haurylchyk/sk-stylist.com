<?php

$title = 'Обо мне | Снежана Королёва';
$page = 'about-me';
$styles = '\views\layouts\allPagesCSSheader.php';
$scripts = '\views\layouts\allPagesJSfooter.php';

include ROOT . '\views\layouts\header.php';
?>

<section class="single-page">
    <div class="section-content bg-pattern white-screen">
        <div class="section-page container">
            <div class="row row-big">
                <div class="col-xxl-6 col-lg-12 col-md-6 content-column">
                    <article>
                        <h1 class="no-top-margin">Обо мне</h1>
                        <p class="paragraph-big">
                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                        </p>
                        <p class="paragraph-big">
                            Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari.
                        </p>
                        <a href="/contacts" class="button-big">Записаться на прием</a>
                    </article>
                </div><!-- .col-xxl-6 -->
                <div class="col-xxl-6 col-lg-12 col-md-6 content-column">
                    <img class="img-full" alt="about us" src="/template/images/aboutus.jpg">
                </div><!-- .col-xxl-6 -->
            </div><!-- .row -->
        </div><!-- .section-page -->
    </div><!-- .section-content -->
</section>

<?php

include ROOT . '\views\layouts\footer.php';
