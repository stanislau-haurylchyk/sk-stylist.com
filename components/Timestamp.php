<?php

class Timestamp {

    public static function getDateFromNews() {
        $db = Db::getConnection();
        $result = $db->query('SELECT date FROM news');

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $dateItem = $result->fetch();

        return $dateItem;
    }

    public static function month() {
        $dateItem = self::getDateFromNews();

        $pattern = '/([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})/';
        $months = '$2';
        $currentMonth = preg_replace($pattern, $months, $dateItem);        

        switch ($currentMonth) {
            case 1: $month = 'Январь' . '<br>';
                break;
            case 2: $month = 'Февраль' . '<br>';
                break;
            case 3: $month = 'Март' . '<br>';
                break;
            case 4: $month = 'Апрель' . '<br>';
                break;
            case 5: $month = 'Май' . '<br>';
                break;
            case 6: $month = 'Июнь' . '<br>';
                break;
            case 7: $month = 'Июль' . '<br>';
                break;
            case 8: $month = "Август" . '<br>';
                break;
            case 9: $month = 'Сентябрь' . '<br>';
                break;
            case 10: $month = 'Октябрь' . '<br>';
                break;
            case 11: $month = 'Ноябрь' . '<br>';
                break;
            case 12: $month = 'Декабрь' . '<br>';
                break;
        }
        return $month;
    }

}
